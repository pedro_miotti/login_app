# DEPENDENCIES 
<br>

#### Express 
<br>

    yarn add --save express //Server


#### Database Handler 
<br>

    yarn add --save mongoose //Mongo Handler

#### Body-Parser
<br>

    yarn add body-parser

#### Bcrypt
<br>

    yarn add --save bcryptjs //Hash password

#### Passport
<br>

    yarn add --save passport passport-local passport-jwt //Autenticacao de usuario ( Com 2 strategys JWT e LOCAL )

#### Express-Sessions 
<br>

    yarn add --save express-sessions // Utilizar sessoes 


#### Concurrently
<br>

    yarn add --save-D concurrently // Run multiple commands


#### Nodemailer
<br>

    yarn add --save nodemailer // Send email through the server

    
#### Nodemailer express handlebars
<br>

    yarn add --save nodemailer-express-handlebars // Create templates for the emails

    

